# WunderMobTest
1) Describe possible performance optimizations for your Code.
- Implemented in MVVM and optimised code for good performance by updating UI on main threads and loading images through cache. 

2) Which things could be done better, than you've done it?
- We can use Rx, Alamofire for more better things and can implement loader while updating UI.

