//
//  APIManager.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//

import UIKit

class APIManager{
    
    let carsDataURL = "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars.json"
    let carDetailDataURL = "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars/"
    let appdelegate = UIApplication.shared.delegate as! AppDelegate

    //MARK: - Cars List
    func getCarsList(completion: @escaping (_ car: [Car]?, _ error: Error?) -> Void) {
        
        getJSONFromURL(urlString: carsDataURL) { [weak self](data, error) in
            guard let data = data, error == nil else {
                print("Failed to get data")
                return completion(nil, error)
            }
            self?.createCarObjectWith(json: data, completion: { (carsArray, error) in
                if let error = error {
                    print("Failed to convert data")
                    return completion(nil, error)
                }
                return completion(carsArray, nil)
            })
        }
    }
    
    //MARK: - Car details
    func getCarDetails(cID: String, completion: @escaping (_ car: Car?, _ error: Error?) -> Void) {
        getJSONFromURL(urlString: "\(carDetailDataURL)\(cID)") { [weak self](data, error) in
            guard let data = data, error == nil else {
                print("Failed to get data")
                return completion(nil, error)
            }
            self?.createCarDetailsObjectWith(json: data, completion: { (carDetails, error) in
                if let error = error {
                    print("Failed to convert data")
                    return completion(nil, error)
                }
                return completion(carDetails, nil)
            })
        }
    }
    
    //MARK: - Vehicle Image
    func getImageFromWeb(_ urlString: String, closure: @escaping (UIImage?) -> ()) {
        
        var vehicleImage: UIImage!
        if let imageFromCache = appdelegate.imageCache.object(forKey: urlString as NSString){
            print("from cache")
            return closure(imageFromCache)
        }
        
        getJSONFromURL(urlString: urlString) { [weak self](data, error) in
            
            guard let data = data, error == nil else {
                print("Failed to get data")
                return closure(nil)
            }
            vehicleImage = UIImage(data: data)
            self?.appdelegate.imageCache.setObject(vehicleImage!, forKey: urlString as NSString)
            return closure(vehicleImage)
        }
    }
    
    //MARK: - Quick-Rent
    func quickRentAvailableRespones(id: String, completion: @escaping(_ data: [String: Any]?, _ error: Error?) -> Void){
        
        let token = "df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa"
        let parameters = ["carId" : id]
        let url = URL(string: "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else { return }
            guard let data = data else { return }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    completion(json, error)
                }
            } catch let error {
                print(error.localizedDescription)
                completion(nil, error)
            }
        })
        task.resume()
    }
}

extension APIManager {
    //MARK: - JSON Data
    private func getJSONFromURL(urlString: String, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: urlString) else {
            print("Error: Cannot create URL from string")
            return
        }
        let urlRequest = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, _, error) in
            guard error == nil else {
                return completion(nil, error)
            }
            guard let responseData = data else {
                return completion(nil, error)
            }
            completion(responseData, nil)
        }
        task.resume()
    }
    
    //MARK: - Car Objects
    private func createCarObjectWith(json: Data, completion: @escaping (_ data: [Car]?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let cars = try decoder.decode([Car].self, from: json)
            return completion(cars, nil)
        } catch let error {
            print("Error: \(error.localizedDescription)")
            return completion(nil, error)
        }
    }
    
    //MARK: - Car Details Object
    private func createCarDetailsObjectWith(json: Data, completion: @escaping (_ data: Car?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let carDetails = try decoder.decode(Car.self, from: json)
            return completion(carDetails, nil)
        } catch let error {
            print("Error: \(error.localizedDescription)")
            return completion(nil, error)
        }
    }
}



