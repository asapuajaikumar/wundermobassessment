//
//  WMCarViewModel.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//

import Foundation

struct WMCarViewModel{
    
    let carID: Int
    var carTitle: String
    let lat: Double
    let lon: Double
    let licensePlate: String
    let vehicleTypeImageUrl: String
    let isClean: Bool
    let isDamaged: Bool
    let fuelLevel: Int
    let vehicleStateId: Int
    let hardwareId: String
    let vehicleTypeId: Int
    let pricingTime: String
    let pricingParking: String
    let isActivatedByHardware: Bool
    let locationId: Int
    let address: String
    let zipCode: String
    let city: String
    let reservationState: Int
    let damageDescription: String
    
    
    init(car: Car) {
        self.carID = car.carId ?? 0
        self.carTitle = car.title ?? ""
        self.lat = car.lat ?? 0
        self.lon = car.lon ?? 0
        self.licensePlate = car.licencePlate ?? ""
        self.vehicleTypeImageUrl = car.vehicleTypeImageUrl ?? ""
        self.isClean = car.isClean ?? false
        self.isDamaged = car.isDamaged ?? false
        self.fuelLevel = car.fuelLevel ?? 0
        self.vehicleStateId = car.vehicleStateId ?? 0
        self.hardwareId = car.hardwareId ?? ""
        self.vehicleTypeId = car.vehicleTypeId ?? 0
        self.pricingTime = car.pricingTime ?? ""
        self.pricingParking = car.pricingParking ?? ""
        self.isActivatedByHardware = car.isActivatedByHardware ?? false
        self.locationId = car.locationId ?? 0
        self.address = car.address ?? ""
        self.zipCode = car.zipCode ?? ""
        self.city = car.city ?? ""
        self.reservationState = car.reservationState ?? 0
        self.damageDescription = car.damageDescription ?? ""
        self.carTitle = self.carTitle.isEmpty ? "No Title" : self.carTitle
    }
}

