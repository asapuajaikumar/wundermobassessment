//
//  Car.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//

import Foundation

struct Car: Codable{
    
    let carId: Int?
    let title: String?
    let isClean: Bool?
    let isDamaged: Bool?
    let licencePlate: String?
    let fuelLevel: Int?
    let vehicleStateId: Int?
    let hardwareId: String?
    let vehicleTypeId: Int?
    let pricingTime: String?
    let pricingParking: String?
    let isActivatedByHardware: Bool?
    let locationId: Int?
    let address: String?
    let zipCode: String?
    let city: String?
    let lat: Double?
    let lon: Double?
    let reservationState: Int?
    let damageDescription: String?
    let vehicleTypeImageUrl: String?
}

