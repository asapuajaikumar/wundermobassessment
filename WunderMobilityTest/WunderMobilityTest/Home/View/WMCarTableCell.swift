//
//  WMCarTableCell.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//

import UIKit

class WMCarTableCell: UITableViewCell {
    
    var carViewModel: WMCarViewModel! {
        didSet {
            textLabel?.text = "Car ID : \(carViewModel.carID)"
            detailTextLabel?.text = "Title: \(carViewModel.carTitle)\nFuelLevel: \(carViewModel.fuelLevel)\nVehicle State ID: \(carViewModel.vehicleStateId)\nLicence Plate: \(carViewModel.licensePlate)\nHardware ID: \(carViewModel.hardwareId)\nVehicleType ID: \(carViewModel.vehicleTypeId)\nPricing Time: \(carViewModel.pricingTime)\nPricing Praking: \(carViewModel.pricingParking)\nIs Activated by H/W: \(carViewModel.isActivatedByHardware)\nLocation ID: \(carViewModel.locationId)\nAddress: \(carViewModel.address)\nZipCode: \(carViewModel.zipCode)\nCity: \(carViewModel.city)\nReservation State: \(carViewModel.reservationState)\nIs cleaned?: \(carViewModel.isClean)\nIs damanged?: \(carViewModel.isDamaged)\nDamage description: \(carViewModel.damageDescription)"
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        // cell customization
        textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        textLabel?.numberOfLines = 0
        detailTextLabel?.textColor = .black
        detailTextLabel?.numberOfLines = 0
        detailTextLabel?.font = UIFont.systemFont(ofSize: 18, weight: .light)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}

