//
//  WMCarMapVC.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//

import UIKit
import MapKit

class WMCarMapVC: UIViewController, CLLocationManagerDelegate{
    
    @IBOutlet weak var wunderMap: MKMapView!
    private(set) var wmCarViewModel: [WMCarViewModel]?
    private let apiManager = APIManager()
    let locationManager = CLLocationManager()
    var selectedCarID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        wunderMap.userLocation.title = ""
        wunderMap.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchAllCarsList()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(locations[0].coordinate.latitude, locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002))
        wunderMap.setRegion(region, animated: true)
        wunderMap.showsUserLocation = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Unable to access your current location")
    }
}


extension WMCarMapVC: MKMapViewDelegate{
    
    //Add annotations on Map
    private func addAnnotationsWithCoordinates(lat: Double, lon: Double, carTitle: String, carID: Int){
        let annot = MKPointAnnotation()
        annot.coordinate = CLLocationCoordinate2DMake(lat, lon)
        annot.title = carTitle
        annot.subtitle = "\(carID)"
        wunderMap.addAnnotation(annot)
    }
    
    //View for Annoatation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        annotationView.canShowCallout = true
        annotationView.rightCalloutAccessoryView = UIButton.init(type: UIButton.ButtonType.infoLight)
        
        return annotationView
    }
    
    //Handle Selected Annoation
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let carID = (view.annotation?.subtitle)!{
            let annotationsToRemove = mapView.annotations.filter { $0 !== view.annotation }
            mapView.removeAnnotations(annotationsToRemove)
            selectedCarID = carID
        }
    }
    
    //Accessory control tapped on Map
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        performSegue(withIdentifier: "detailView", sender: self)
    }
    
    // Segue preparation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailVC = segue.destination as! WMCarDetailVC
        detailVC.carID = selectedCarID
    }
    
    //Fetching all cars data
    private func fetchAllCarsList() {
        apiManager.getCarsList() { [weak self](cars, error) in
            if let error = error {
                print("Get Car error: \(error.localizedDescription)")
                return
            }
            guard let cars = cars  else { return }
            self?.wmCarViewModel = cars.map({return
                WMCarViewModel(car: $0)
            })
            DispatchQueue.main.async {
                self?.loadPins(cars: (self?.wmCarViewModel)!)
            }
        }
    }
    
    //Loading annotations
    func loadPins(cars: [WMCarViewModel]){
        for individualCar in cars {
            DispatchQueue.main.async {
                self.addAnnotationsWithCoordinates(lat: individualCar.lat, lon: individualCar.lon, carTitle: individualCar.carTitle, carID: individualCar.carID)
            }
        }
    }
}

