//
//  WMCarDetailVC.swift
//  WunderMobilityTest
//
//  Created by Ajaikumar on 23/08/19.
//  Copyright © 2019 Ajaikumar. All rights reserved.
//


import UIKit

class WMCarDetailVC: UIViewController{
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carDetailTable: UITableView!
    private let apiManager = APIManager()
    private(set) var wmCarViewModel: WMCarViewModel?
    
    var customView: UIView!
    var carID = ""
    let cellId = "cellId"
    
    override func viewDidLoad() {
        
        //Registering table
        carDetailTable.register(WMCarTableCell.self, forCellReuseIdentifier: cellId)
        
        //Fetch Car details with ID
        fetchDetailViewDataWith(carID: carID)
        
        //Table Footer View
        customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        customView.backgroundColor = UIColor.lightGray
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        button.setTitle("quick-rent", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(quickRentAction), for: .touchUpInside)
        customView.addSubview(button)
    }
    
    //MARK: - Handle Quick-Rent Action
    @objc func quickRentAction(_ sender: UIButton!) {
        
        apiManager.quickRentAvailableRespones(id: carID) { [weak self](response, error) in
            //Error Handling
            if let error = error {
                DispatchQueue.main.async {
                    self?.showAlertWithMessage(message: error.localizedDescription)
                }
                return
            }
            //Success Handling
            guard let response = response else { return }
            let carid = response["carId"] ?? ""
            let cost = response["cost"] ?? ""
            DispatchQueue.main.async {
                self?.showAlertWithMessage(message: "Successfully rented with \(carid)\nCost: \(cost)")
            }
        }
    }
    
    //MARK: - Fetch Car details
    private func fetchDetailViewDataWith(carID: String){
        
        if carID.isEmpty{ return }
        
        apiManager.getCarDetails(cID: carID) { [weak self](carDetails, error) in
            
            if let error = error {
                print("Get Car error: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    self?.showAlertWithMessage(message: error.localizedDescription)
                }
                return
            }
            guard let carDetails = carDetails  else { return }
            self?.wmCarViewModel = WMCarViewModel(car: carDetails)
            
            self?.apiManager.getImageFromWeb((self?.wmCarViewModel?.vehicleTypeImageUrl)!, closure: { (vehicleImage) in
                DispatchQueue.main.async {
                    self?.carImageView.image = vehicleImage
                    self?.carDetailTable.tableFooterView = self?.customView
                    self?.carDetailTable.reloadData()
                }
            })
        }
    }
    
    //MARK: - Alert handling
    func showAlertWithMessage(message: String){
        
        let alertController = UIAlertController(title: "WunderMob", message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension WMCarDetailVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (wmCarViewModel != nil) ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! WMCarTableCell
        cell.carViewModel = wmCarViewModel
        return cell
    }
}



